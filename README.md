# PagedChests

**PagedChests** is a plugin that attempts to give your players a little more flexibility then it comes to how
their storage is handled. Simply put, it makes chests bigger by adding pages to them - so while the bottom row
of item slots in a chest is taken up by the pager, your users will end up with chests that can store a lot more
items!

This plugin is in the very early stages, and is just a concept right now.

---

## Project Status

This project is still in the early stages, and it's not ready to be used just yet.

Please note that as this plugin is written in Kotlin, you'll need 
[KotlinPlugin](https://www.spigotmc.org/resources/kotlinplugin-allow-to-use-kotlin-corountines-in-your-plugins.70526/) 
installed to use it.

## Building the project

This project makes use of **Gradle**. Open a terminal and run the following commands from the project directory:

* **Windows**: `gradlew jar`
* **Linux/Mac**: `./gradlew jar`

You will require a JDK installed for this - version 8 or later.

Once this task has been run, you will find a JAR file in `build/lib`, named `PagedChests-<version>.jar`. This is the
plugin - drop it in your `plugins/` folder to get started.

## Questions & Contributions

Questions, concerns, ideas? [Open an issue!](https://gitlab.com/gserv.me/pagedchests/issues/new) Pull requests are
welcome, once the project has gotten off the ground.
