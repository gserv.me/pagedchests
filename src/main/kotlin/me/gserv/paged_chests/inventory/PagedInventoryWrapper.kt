package me.gserv.paged_chests.inventory

import org.bukkit.inventory.Inventory
import java.lang.ref.WeakReference

class PagedInventoryWrapper(
        val inventory: Inventory,  // The actual physical inventory being wrapped
        val page: Number,  // The current page number
        val totalPages: Number,  // The total number of pages
        previous: PagedInventoryWrapper?,  // Paged inventory/null to refer to previous page
        next: PagedInventoryWrapper?,  // Paged inventory/null to refer to next page
        chest: PagedChestWrapper  // Wrapped chest that owns this paged inventory
) {

    // We use WeakReferences here to make garbage collection a bit easier
    private val previousRef = WeakReference<PagedInventoryWrapper>(previous)
    private val nextRef = WeakReference<PagedInventoryWrapper>(next)
    private val chestRef = WeakReference<PagedChestWrapper>(chest)

    // More friendly wrappers
    private val previousInv: PagedInventoryWrapper?
        get() = this.previousRef.get()

    private val nextInv: PagedInventoryWrapper?
        get() = this.nextRef.get()

    private val chest: PagedChestWrapper?
        get() = this.chestRef.get()
}
